using mpc, pptools

function threshold_test()

  println("\33[2J") 
  println("======================================")
  println(" Running threshold_test from jovedemo ")
  println("======================================")
  println(" using:  - PlatformCommander (https://gitlab.com/KWM-PSY/platform-commander)")
  println("         - mpc (https://gitlab.com/KWM-PSY/mpc/)")
  println("         - pptools (https://gitlab.com/dr_e/pptools)")
  sleep(1)
  println("")
  println("======================================")
  println("         estimation procedure         ")
  println("======================================")
  println(" Select estimation procedure")
  println(" 1 - staircase")
  println(" 2 - other procedures")
  prod = mpc.get_input(" Selection", [1,2])
  if prod != 1
    println("")
    println(" => Sorry, at the moment only staircases are implemented")
    return
  end

  println("")
  println("======================================")
  println("             define motion            ")
  println("======================================")
  msg = " Enter stimulation frequency [0.01-5]"
  freq = convert(Float64,
                 mpc.get_input(msg, 
                               [0.01, 5.0]))

  msg = " Select axis (1=lateral, 2=heave, 3=surge, 4=yaw, 5=pitch, 6=roll)"
  axis = convert(Int64,
                 mpc.get_input(msg,
                               [1, 6]))

  println("======================================")
  println("           define staircase           ")
  println("======================================")
  
  println(" 1 = 3-down/1-up with stop @ 20 reversals")
  println(" 2 = define staircase by hand")
  msg = "Select"
  staircase = convert(Int64,
                 mpc.get_input(msg,
                               [1, 2]))

  if staircase == 1
    sc = pptools.cfg_staircase(up = 1,
                               down = 3,
                               stop_rule = "reversals")
  else
    up = convert(Int64,
                 mpc.get_input("up",
                               [1, 10]))
    down = convert(Int64,
                   mpc.get_input("down",
                                 [1, 10]))
    step_size_up = convert(Float64,
                           mpc.get_input("step size up",
                                         [0.1, 50.0]))
    step_size_down = convert(Float64,
                             mpc.get_input("step size down",
                                           [0.1, 50.0]))
    stop_rule = mpc.get_input("stop rule",
                              ["trials", "reversals"])
    stop_value = convert(Int64,
                         mpc.get_input("stop value",
                                       [1, 1000]))
    start_value = convert(Float64,
                          mpc.get_input("start intensity",
                                        [0.1, 100.0]))   

    sc = pptools.cfg_staircase(up = up,
                               down = down,
                               step_size_up = step_size_up,
                               step_size_down = step_size_down,
                               stop_rule = stop_rule,
                               stop_value = stop_value,
                               start_value = start_value)
                               
  end

  # start stession & engage
  cfg_session = mpc.login(mpc.short(common_params = mpc.common_login_params(topic = "HS2022")))

  cfg_session = mpc.open_audio(cfg_session, 1)
  cfg_session = mpc.activate_hw(cfg_session,
                                [mpc.gamepad(nb_buttons = 2,
                                             button_names = ["square", "circle"])])


  println("\33[2J") 
  println(" Hit Return to start estimation procedure ")
  println("==========================================")
  readline()  

  while ~sc.stop
    motion_profile = mpc.get_sinusoidal(freq,
                                        sc.x[end],
                                        axis,
                                        [0.0,670.0,0.0,0.0,0.0,0.0])
    cfg_motion_r = mpc.motion_template(dof = motion_profile.dof)

    motion_profile = mpc.get_sinusoidal(freq,
                                        -sc.x[end],
                                        axis,
                                        [0.0,670.0,0.0,0.0,0.0,0.0])
    cfg_motion_l = mpc.motion_template(dof = motion_profile.dof)

    
    # Todo: start white noise
    direction = sign(randn(1)[1])
    if direction >= 0
      println("[  Moving $(sc.x[end])  ]")
      mpc.start_short_motion(cfg_session, cfg_motion_r)
    else
      println("[  Moving $(sc.x[end])  ]")
      mpc.start_short_motion(cfg_session, cfg_motion_l)
    end

    println("[  Waiting for button press  ]")
    cfg_session, button_time, button_down = mpc.get_button_press(cfg_session)

    sleep(1/freq)

    # 1) check if button press is correct 
    # 2) adjust peak_velocity
    if direction >= 0
      if button_down == "square"
        println("[  Correct  ]")
        sc = pptools.update_sc!(sc, 1)
      else
        println("[  Incorrect  ]")
        sc = pptools.update_sc!(sc, 0)
      end
    else
      if button_down == "circle"
        println("[  Correct  ]")
        sc = pptools.update_sc!(sc, 1)
      else
        println("[  Incorrect  ]")
        sc = pptools.update_sc!(sc, 0)
      end
    end

    # move back to center
    if direction >= 0
      println("[  Moving to center  ]")
      mpc.start_short_motion(cfg_session, cfg_motion_r, 1)
    else
      println("[  Moving to center  ]")
      mpc.start_short_motion(cfg_session, cfg_motion_l, 1)
    end
    
    sleep(1/freq)
  end

  println("======================================")
  println("     Estimation procedure done        ")
  println("")
  println("======================================")
  println(" Enter filename: ")
  datafile = readline()
#  open(datafile, "w") do f
#    for x = 1:size(sc.x,1)
#      write(f, sc.x[x], ",", 
#	    sc.response[x], ",", 
#	    sc.direction[x], ",", 
#	    sc.reversal[x],  "\n")
#    end
#  end
  println(" Saving data")
  println(" Hit return to park platform")
  readline()
end

threshold_test()
